using DigitalExamProject.Config;
using DigitalExamProject.Students.Application;
using DigitalExamProject.Students.Domain;
using DigitalExamTestProject.Helper;


namespace DigitalExamTestProject.Students
{
    [TestClass]
    public class StudentTest : AbstractEntityTest
    {
        private const string STUDENT_AMELIE_NAME = "Amelie";
        private const int STUDENT_AMELIE_MATR_NO = 1234;
        private Student amelie;

        private const string STUDENT_GEORG_NAME = "Georg";
        private const int STUDENT_GEORG_MATR_NO = 5678;
        private Student georg;

        private StudentRepositoryService studentRepositoryService;
        private DigitalExamsGlobalContext globalContext;


        [TestInitialize]
        public void SetUp()
        {
            globalContext = new();
            PrepareContext(globalContext);
            studentRepositoryService = new StudentRepositoryService(globalContext);

            amelie = new Student(STUDENT_AMELIE_NAME, STUDENT_AMELIE_MATR_NO);
            georg = new Student(STUDENT_GEORG_NAME, STUDENT_GEORG_MATR_NO);
        }

        [TestCleanup]
        public void TearDown()
        {
            globalContext.Database.EnsureDeleted();
            globalContext.SaveChanges();
            globalContext.Dispose();
        }


        [TestMethod]
        public void TestStoreAndLoad()
        {
            // given
            // when
            studentRepositoryService.Add(georg);
            studentRepositoryService.Add(amelie);
            studentRepositoryService.SaveChanges();

            // then
            Assert.IsTrue(studentRepositoryService.ExistsById(georg.Id));
            Assert.IsTrue(studentRepositoryService.ExistsById(amelie.Id));
            Student? newGeorg = studentRepositoryService.FindById(georg.Id);
            Student? newAmelie = studentRepositoryService.FindById(amelie.Id);
            Assert.IsNotNull(newGeorg);
            Assert.IsNotNull(newAmelie);
            Assert.AreEqual(STUDENT_GEORG_MATR_NO, newGeorg?.MatrikelNumber);
        }

        [TestMethod]
        public void TestDeleteInvalidId()
        {
            studentRepositoryService.DeleteById(Guid.NewGuid());
        }

        [TestMethod]
        public void TestFindByInvalidId()
        {
            Assert.IsNull(studentRepositoryService.FindById(Guid.NewGuid()));
        }


        [TestMethod]
        public void TestAddAddress()
        {
            // given
            studentRepositoryService.Add(amelie);
            studentRepositoryService.SaveChanges();

            // when 
            Student? newAmelie = studentRepositoryService.FindById(amelie.Id);
            Address address1 = new Address("Hauptstr.", 3, 78565, "Kaufbeuren");
            newAmelie.HomeAddress = address1;
            studentRepositoryService.SaveChanges();

            // then
            newAmelie = studentRepositoryService.FindById(amelie.Id);
            Assert.AreEqual(address1, newAmelie.HomeAddress);
        }


        [TestMethod]
        public void TestFindByName()
        {
            // given
            studentRepositoryService.Add(amelie);
            studentRepositoryService.Add(georg);
            Student amelie2 = new Student(STUDENT_AMELIE_NAME, 5555);
            studentRepositoryService.Add(amelie2);
            studentRepositoryService.SaveChanges();

            // when
            List<Student> amalies = studentRepositoryService.FindByName(STUDENT_AMELIE_NAME);
            List<Student> georgs = studentRepositoryService.FindByName(STUDENT_GEORG_NAME);
            List<Student> gollums = studentRepositoryService.FindByName("Gollum");

            // then
            Assert.AreEqual(2, amalies.Count);
            Assert.AreEqual(1, georgs.Count);
            Assert.AreEqual(0, gollums.Count);
        }


        [TestMethod]
        public void TestDeleteAll()
        {
            // given
            studentRepositoryService.Add(amelie);
            studentRepositoryService.Add(georg);
            studentRepositoryService.SaveChanges();

            // when
            studentRepositoryService.DeleteAll();
            List<Student> students = studentRepositoryService.GetAll();

            // then
            Assert.AreEqual(0, students.Count);
        }
    }
}