using DigitalExamProject.Students.Domain;

namespace DigitalExamTestProject.Students
{
    [TestClass]
    public class AddressTest
    {
        [TestMethod]
        public void TestAdressEquality()
        {   
            // given
            // when
            Address? address1 = new Address("Hauptstr.", 3, 78565, "Kaufbeuren");
            Address? address2 = new Address("Hauptstr.", 3, 78565, "Kaufbeuren");
            Address? address3 = null;
            Address? address4 = new Address("Hauptstr.", 4, 78565, "Kaufbeuren");

            // then
            Assert.AreEqual(address1, address2);
            Assert.AreNotEqual(address1, address3);
            Assert.AreNotEqual(address1, address4);
        }
    }
}