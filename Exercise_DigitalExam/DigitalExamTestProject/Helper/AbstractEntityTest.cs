﻿using Microsoft.EntityFrameworkCore;

namespace DigitalExamTestProject.Helper
{
    public abstract class AbstractEntityTest
    {
        protected void PrepareContext( DbContext dbContext ) {
            dbContext.Database.EnsureDeleted();
            dbContext.SaveChanges();
            dbContext.Database.EnsureCreated();
            dbContext.SaveChanges();
        }
    }
}
