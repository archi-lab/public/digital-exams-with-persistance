using DigitalExamProject.Students.Domain;
using DigitalExamProject.Exams.Domain;
using System.Collections.ObjectModel;
using DigitalExamTestProject.Helper;
using DigitalExamProject.Config;
using DigitalExamProject.Exams.Application;

namespace DigitalExamTestProject.Exams
{
    [TestClass]
    public class DigitalExamTest : AbstractEntityTest
    {
        private const string STUDENT_AMELIE_NAME = "Amelie";
        private const int STUDENT_AMELIE_MATR_NO = 1234;
        private Student amelie;

        private const string STUDENT_GEORG_NAME = "Georg";
        private const int STUDENT_GEORG_MATR_NO = 5678;
        private Student georg;

        private const string EXAM_ST1_TITLE = "ST1";
        private const string EXAM_ST1_TEXT = "ST1 Klausur";
        private DateTime EXAM_ST1_date;
        private DigitalExam st1;

        private const string EXAM_ST2_TITLE = "ST2";
        private const string EXAM_ST2_TEXT = "ST2 Klausur";
        private DateTime EXAM_ST2_date;
        private DigitalExam st2;

        private const string EXAM_DDD_TITLE = "DDD";
        private const string EXAM_DDD_TEXT = "DDD Klausur";
        private DateTime EXAM_DDD_date;
        private DigitalExam ddd;

        private DigitalExamRepositoryService digitalExamRepositoryService;
        private StudentRepositoryService studentRepositoryService;
        private DigitalExamsGlobalContext globalContext;


        [TestInitialize]
        public void SetUp()
        {
            globalContext = new();
            PrepareContext(globalContext);
            digitalExamRepositoryService = new DigitalExamRepositoryService(globalContext);
            studentRepositoryService = new StudentRepositoryService(globalContext);

            EXAM_ST1_date = DateTime.Now.AddDays(6) ;
            st1 = new DigitalExam(EXAM_ST1_TITLE, EXAM_ST1_TEXT, EXAM_ST1_date);
            EXAM_ST2_date = DateTime.Now.AddDays(30);
            st2 = new DigitalExam(EXAM_ST2_TITLE, EXAM_ST2_TEXT, EXAM_ST2_date);
            EXAM_DDD_date = DateTime.Now.AddDays(8);
            ddd = new DigitalExam(EXAM_DDD_TITLE, EXAM_DDD_TEXT, EXAM_DDD_date);

            amelie = new Student(STUDENT_AMELIE_NAME, STUDENT_AMELIE_MATR_NO);
            amelie.HomeAddress = new Address("Hauptstr.", 3, 78565, "Kaufbeuren");
            georg = new Student(STUDENT_GEORG_NAME, STUDENT_GEORG_MATR_NO);
            studentRepositoryService.Add(amelie);
            studentRepositoryService.Add(georg);
            studentRepositoryService.SaveChanges();
        }

        [TestCleanup]
        public void TearDown()
        {
            globalContext.Database.EnsureDeleted();
            globalContext.SaveChanges();
            globalContext.Dispose();
        }


        [TestMethod]
        public void TestJustStoreAndLoadOfAnExam()
        {
            // given
            // when
            digitalExamRepositoryService.Add(st1);
            digitalExamRepositoryService.Add(ddd);
            digitalExamRepositoryService.SaveChanges();

            // then
            Assert.IsTrue(digitalExamRepositoryService.ExistsById(st1.Id));
            Assert.IsTrue(digitalExamRepositoryService.ExistsById(ddd.Id));
            DigitalExam? newST1 = digitalExamRepositoryService.FindById(st1.Id);
            DigitalExam? newDDD = digitalExamRepositoryService.FindById(ddd.Id);
            Assert.IsNotNull(newST1);
            Assert.IsNotNull(newDDD);
            Assert.AreEqual(EXAM_ST1_TEXT, newST1?.Text);
            Assert.AreEqual(EXAM_DDD_TITLE, newDDD?.Title);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestRegisterStudentImpossible()
        {
            // given
            amelie = new Student(STUDENT_AMELIE_NAME, STUDENT_AMELIE_MATR_NO);

            // when
            st1.Register(amelie);
        }
    

        [TestMethod]
        public void TestRegisterStudent()
        {
            // given
            // when
            digitalExamRepositoryService.Add(st2);
            digitalExamRepositoryService.Add(ddd);
            ddd.Register(amelie);
            st2.Register(georg);
            st2.Register(amelie);
            digitalExamRepositoryService.SaveChanges();

            // then
            DigitalExam? newDDD = digitalExamRepositoryService.FindById(ddd.Id);
            DigitalExam? newST2 = digitalExamRepositoryService.FindById(st2.Id);
            ReadOnlyCollection<Student> registeredStudentsDDD = newDDD.GetRegisteredStudents();
            ReadOnlyCollection<Student> registeredStudentsST2 = newST2.GetRegisteredStudents();
            Assert.AreEqual(1, registeredStudentsDDD.Count());
            Assert.IsTrue(newDDD.IsRegistered(amelie));
            Assert.IsTrue(newST2.IsRegistered(amelie));
            Assert.IsTrue(newST2.IsRegistered(georg));
            Assert.AreEqual(new Address("Hauptstr.", 3, 78565, "Kaufbeuren"), registeredStudentsDDD.First().HomeAddress);
            Assert.AreEqual(2, registeredStudentsST2.Count());
        }


        [TestMethod]
        public void TestRegisterStudentFromMatrNumber()
        {
            // given
            digitalExamRepositoryService.Add(ddd);
            digitalExamRepositoryService.SaveChanges();

            // when
            DigitalExam? newDDD = digitalExamRepositoryService.FindById(ddd.Id);
            ReadOnlyCollection<Student> registeredStudentsDDD = newDDD.GetRegisteredStudents();
            Assert.AreEqual(0, registeredStudentsDDD.Count());
            var students = studentRepositoryService.FindByMatrNumber(STUDENT_GEORG_MATR_NO);
            Assert.AreEqual(1, students.Count());
            newDDD.Register(students.ElementAt(0));
            digitalExamRepositoryService.SaveChanges();

            // then
            newDDD = digitalExamRepositoryService.FindById(ddd.Id);
            registeredStudentsDDD = newDDD.GetRegisteredStudents();
            Assert.AreEqual(1, registeredStudentsDDD.Count());
            Assert.AreEqual(STUDENT_GEORG_NAME, registeredStudentsDDD.ElementAt(0).Name);
        }



        [TestMethod]
        public void TestUnregisterStudent()
        {
            // given
            digitalExamRepositoryService.Add(st2);
            st2.Register(georg);
            st2.Register(amelie);
            digitalExamRepositoryService.SaveChanges();

            // when
            DigitalExam? newST2 = digitalExamRepositoryService.FindById(st2.Id);
            Assert.AreEqual(2, newST2.GetRegisteredStudents().Count());
            st2.Unregister(amelie);
            Assert.IsFalse(newST2.IsRegistered(amelie));
            Assert.AreEqual(1, newST2.GetRegisteredStudents().Count());
            digitalExamRepositoryService.SaveChanges();

            // then
            newST2 = digitalExamRepositoryService.FindById(st2.Id);
            Assert.AreEqual(1, newST2.GetRegisteredStudents().Count());
            Assert.IsTrue(newST2.IsRegistered(georg));
        }


        [TestMethod]
        public void TestDeleteAndStudentsStillThere()
        {
            // given
            digitalExamRepositoryService.Add(st2);
            st2.Register(georg);
            st2.Register(amelie);
            digitalExamRepositoryService.SaveChanges();

            // when
            digitalExamRepositoryService.DeleteById(st2.Id);
            digitalExamRepositoryService.SaveChanges();

            // then
            Assert.IsTrue(studentRepositoryService.ExistsById(georg.Id));
            Assert.IsTrue(studentRepositoryService.ExistsById(amelie.Id));
        }


        [TestMethod]
        public void TestSubmitSolution()
        {
            // given
            digitalExamRepositoryService.Add(st2);
            st2.Register(georg);
            st2.Register(amelie);

            // when
            st2.SubmitSolution(georg, "Das ist Georgs L�sung");
            st2.SubmitSolution(amelie, "Das ist Amelies L�sung");
            st2.SubmitSolution(amelie, "Das ist Amelies zweite L�sung");
            digitalExamRepositoryService.SaveChanges();

            // then
            DigitalExam? newST2 = digitalExamRepositoryService.FindById(st2.Id);
            Assert.AreEqual(3, newST2.GetSolutions().Count());
        }


        [TestMethod]
        public void TestCascadingDeleteSolutions()
        {
            // given
            digitalExamRepositoryService.Add(st2);
            st2.Register(georg);
            st2.Register(amelie);
            st2.SubmitSolution(georg, "Das ist eine L�sung");
            st2.SubmitSolution(amelie, "Das ist auch eine L�sung");
            digitalExamRepositoryService.SaveChanges();
            Assert.AreEqual(1, digitalExamRepositoryService.CountAll());

            // when
            digitalExamRepositoryService.DeleteById(st2.Id);
            digitalExamRepositoryService.SaveChanges();

            // then
            Assert.AreEqual(0, digitalExamRepositoryService.CountAll());
        }


        [TestMethod]
        public void TestCascadingDeleteAllExams()
        {
            // given
            digitalExamRepositoryService.Add(st1);
            digitalExamRepositoryService.Add(st2);
            digitalExamRepositoryService.SaveChanges();
            Assert.AreEqual(2, digitalExamRepositoryService.CountAll());

            // when
            digitalExamRepositoryService.DeleteAll();

            // then
            Assert.AreEqual(0, digitalExamRepositoryService.CountAll());
        }


        [TestMethod]
        public void TestGradeSolution()
        {
            // given
            digitalExamRepositoryService.Add(st2);
            st2.Register(georg);
            st2.Register(amelie);

            // when
            st2.SubmitSolution(georg, "Georgs L�sung");
            st2.SubmitSolution(amelie, "Amelies L�sung");
            ReadOnlyCollection<Solution> solutions = st2.GetSolutions();
            Assert.AreEqual(2, solutions.Count());
            foreach (Solution solution in solutions)
            {
                solution.Grading = Grading.FromString($"Gut gemacht: {solution.Text.Length}");
            } 
            digitalExamRepositoryService.SaveChanges();

            // then
            DigitalExam newST2 = digitalExamRepositoryService.FindById(st2.Id);
            ReadOnlyCollection<Solution> newSolutions = newST2.GetSolutions();
            Assert.AreEqual(2, newSolutions.Count());
            foreach (Solution solution in newSolutions)
            {
                Assert.AreEqual(solution.Text.Length, solution.Grading.Value.Points);
            }
        }

        [TestMethod]
        public void TestInitializeBaseData()
        {
            // given
            using DigitalExamApplicationService digitalExamApplicationService = new();

            // when
            digitalExamApplicationService.InitializeBaseData();

            // then
            Assert.AreEqual(1,digitalExamApplicationService.allDigitalExamAsDTOs().Count());
        }

    }
}