using DigitalExamProject.Students.Domain;

namespace DigitalExamProject.Exams.Domain
{
    [TestClass]
    public class GradingTest
    {
        [TestMethod]
        public void TestGradingCreation()
        {
            // given
            Grading? grading = Grading.FromString("Well done: 9");

            // when
            string? gradingString = grading.ToString();
            Grading? grading2 = Grading.FromString(gradingString);

            // then
            Assert.AreEqual(grading, grading2);
        }


        [TestMethod]
        public void TestGradingEquality()
        {
            // given
            // when
            Grading grading1 = Grading.FromString("Well done: 9");
            Grading grading2 = Grading.FromString("Bit of a hack: 3");
            Grading grading3 = Grading.FromString("Bit of a hack: 3");


            // then
            Assert.AreEqual(grading2, grading3);
            Assert.AreNotEqual(grading1, grading3);
            Assert.AreEqual(grading1.Points, 9);
            Assert.AreEqual(grading1.Remark, "Well done");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), AllowDerivedTypes = true)]
        public void TestValidationNoPoints()
        {
            // given
            Grading grading = Grading.FromString("bl�mpf");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), AllowDerivedTypes = true)]
        public void TestValidationNoRemark()
        {
            // given
            Grading grading2 = Grading.FromString("10");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), AllowDerivedTypes = true)]
        public void TestValidationTooMany()
        {
            // given
            Grading grading3 = Grading.FromString("bla: fasel: 10");
        }
    }
}