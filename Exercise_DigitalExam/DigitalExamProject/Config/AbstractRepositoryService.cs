﻿namespace DigitalExamProject.Config
{
    public abstract class AbstractRepositoryService : IDisposable
    {
        protected readonly DigitalExamsGlobalContext _context;

        public AbstractRepositoryService(DigitalExamsGlobalContext context)
        {
            this._context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Add(Object entity) 
        { 
            _context.Add(entity); 
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
