﻿using DigitalExamProject.Exams.Domain;
using DigitalExamProject.Students.Domain;
using Microsoft.EntityFrameworkCore;

namespace DigitalExamProject.Config
{
    public class DigitalExamsGlobalContext : DbContext
    {
        public string DbPath { get; }

        public DigitalExamsGlobalContext() : base()
        {
            DbPath = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=DigitalExams;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer($"{DbPath}");

        public DbSet<DigitalExam> DigitalExams { get; set; }

        public DbSet<Student> Students { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("DigitalExams");

            modelBuilder
                .Entity<Student>()
                .ToTable("Student")
                .HasKey(s => s.Id);

            modelBuilder
                .Entity<Student>()
                .OwnsOne<Address>(s => s.HomeAddress);

            modelBuilder
                .Entity<DigitalExam>()
                .ToTable("DigitalExam")
                .HasKey(d => d.Id);

            modelBuilder
                .Entity<DigitalExam>()
                .OwnsMany<ExamRegistration>(d => d.ExamRegistrations)
                .HasOne<Student>(er => er.Student);

            var solutionBuilder = modelBuilder
                .Entity<DigitalExam>()
                .OwnsMany<Solution>(d => d.SubmittedSolutions);
            solutionBuilder
                 .ToTable("Solution")
                 .HasKey(s => s.Id);
            solutionBuilder
                .Property(s => s.Grading)
                .HasConversion(
                    g => g.ToString(),
                    g => Grading.FromString(g));

        }
    }
}
