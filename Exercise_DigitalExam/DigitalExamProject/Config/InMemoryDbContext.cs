﻿using Microsoft.EntityFrameworkCore;

namespace DigitalExamProject.Config
{
    public abstract class InMemoryDbContext : DbContext
    {
        public string DbPath { get; }

        public InMemoryDbContext() : base()
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = Path.Join(path, GetType().Name + ".db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseInMemoryDatabase($"Data Source={DbPath}");
    }
}
