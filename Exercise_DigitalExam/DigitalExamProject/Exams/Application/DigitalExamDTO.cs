﻿using DigitalExamProject.Exams.Domain;
using Microsoft.AspNetCore.Mvc.TagHelpers;

namespace DigitalExamProject.Exams.Application
{
    public class DigitalExamDTO
    {
        public Guid Id { get; set; }
        public string? Title { get; set; }
        public string? Text { get; set; }
        public DateTime? Date { get; set; }
        public int[]? RegisteredMatrNumbers { get; set; }
        public SolutionDTO[]? solutions { get; set; }

        public static DigitalExamDTO? fromEntity(DigitalExam digitalExam)
        {
            if (digitalExam == null) return null;

            DigitalExamDTO digitalExamDTO = new();
            digitalExamDTO.Id = digitalExam.Id;
            digitalExamDTO.Title = digitalExam.Title;
            digitalExamDTO.Text = digitalExam.Text;
            digitalExamDTO.Date = digitalExam.Date;
            var students = digitalExam.GetRegisteredStudents();
            digitalExamDTO.RegisteredMatrNumbers = new int[students.Count];
            for (int i = 0; i < students.Count; i++) 
            {
                digitalExamDTO.RegisteredMatrNumbers[i] = students.ElementAt(i).MatrikelNumber;
            }
            return digitalExamDTO;
        }


        public DigitalExam asEntity()
        {
            DigitalExam digitalExam = new();
            if (this.Title == null || this.Text == null || this.Date == null)
                throw new DigitalExamException("Title, Text, or Date is null!");
            digitalExam.Title = this.Title;
            digitalExam.Text = this.Text;
            digitalExam.Date = this.Date.Value;
            return digitalExam;
        }
    }
}
