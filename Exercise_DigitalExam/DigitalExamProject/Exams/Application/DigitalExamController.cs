﻿using DigitalExamProject.Exams.Domain;
using DigitalExamProject.Students.Application;
using Microsoft.AspNetCore.Mvc;

namespace DigitalExamProject.Exams.Application
{
    [ApiController]
    public class DigitalExamController : ControllerBase
    {


        [HttpGet("/digitalExams")]
        public IActionResult GetDigitalExams()
        {
            using DigitalExamApplicationService digitalExamApplicationService = new();
            List<DigitalExamDTO> dtos = digitalExamApplicationService.allDigitalExamAsDTOs();
            return this.Ok(new JsonResult(dtos.ToArray()));
        }


        [HttpGet("/digitalExams/{exam-id}")]
        public IActionResult GetOneDigitalExam([FromRoute(Name = "exam-id")] Guid examId)
        {
            using DigitalExamApplicationService digitalExamApplicationService = new();
            var dto = digitalExamApplicationService.DigitalExamAsDTOs(examId);
            if (dto == null) return this.NotFound();
            return this.Ok(new JsonResult(dto));
        }


        [HttpPost("/digitalExams")]
        public IActionResult CreateOneDigitalExam([FromBody] DigitalExamDTO digitalExamDTO)
        {
            using DigitalExamApplicationService digitalExamApplicationService = new();
            DigitalExam digitalExam;
            try
            {
                digitalExam = digitalExamDTO.asEntity();
            }
            catch(DigitalExamException e)
            {
                return this.UnprocessableEntity(new JsonResult(e.Message));
            }
            digitalExamApplicationService.persist(digitalExam);
            DigitalExamDTO createdDTO = DigitalExamDTO.fromEntity(digitalExam);
            return this.Created($"/digitalExams/{digitalExam.Id}", new JsonResult(createdDTO));
        }


        [HttpPut("/digitalExams/{exam-id}/students/{mnr}")]
        public IActionResult RegisterStudent([FromRoute(Name = "exam-id")] Guid examId,
                                                 [FromRoute(Name = "mnr")] int matrNumber)
        {
            using DigitalExamApplicationService digitalExamApplicationService = new();
            var exam = digitalExamApplicationService.FindById(examId);
            if (exam == null) return this.NotFound();
            
            using StudentApplicationService studentApplicationService = new();
            var studentList = studentApplicationService.FindByMatrNumber(matrNumber);
            if (studentList.Count != 1) return this.NotFound();

            try
            {
                exam.Register(studentList.ElementAt(0));
                digitalExamApplicationService.SaveChanges();
            }
            catch (InvalidOperationException e)
            {
                return this.Conflict(new JsonResult(e.Message));
            }

            exam = digitalExamApplicationService.FindById(exam.Id);
            var examDTO = DigitalExamDTO.fromEntity(exam);
            return this.Ok(new JsonResult(examDTO));
        }


        [HttpDelete("/digitalExams/{exam-id}/students/{mnr}")]
        public IActionResult UnregisterStudent([FromRoute(Name = "exam-id")] Guid examId,
                                               [FromRoute(Name = "mnr")] int matrNumber)
        {
            using DigitalExamApplicationService digitalExamApplicationService = new();
            var exam = digitalExamApplicationService.FindById(examId);
            if (exam == null) return this.NotFound();

            using StudentApplicationService studentApplicationService = new();
            var studentList = studentApplicationService.FindByMatrNumber(matrNumber);
            if (studentList.Count != 1) return this.NotFound();

            exam.Unregister(studentList.ElementAt(0));
            digitalExamApplicationService.SaveChanges();

            exam = digitalExamApplicationService.FindById(exam.Id);
            var examDTO = DigitalExamDTO.fromEntity(exam);
            return this.Ok(new JsonResult(examDTO));
        }


        /// <summary>
        /// Bit of a hack to initialize some base data. It returns not what you would think it returns 
        /// (200 instead 201). DON'T USE THIS METHOD AS TEMPLATE.
        /// </summary>
        /// <returns></returns>
        [HttpPost("/basedata")]
        public IActionResult InitializeBaseData()
        {
            using DigitalExamApplicationService digitalExamApplicationService = new();
            digitalExamApplicationService.InitializeBaseData();
            return this.Ok();
        }


        /// <summary>
        /// Bit of a hack to delete the base data. DON'T USE THIS METHOD AS TEMPLATE.
        /// </summary>
        /// <returns></returns>
        [HttpDelete("/basedata")]
        public IActionResult DeleteBaseData()
        {
            using DigitalExamApplicationService digitalExamApplicationService = new();
            digitalExamApplicationService.DeleteAllData();
            return this.Ok();
        }

    }
}
