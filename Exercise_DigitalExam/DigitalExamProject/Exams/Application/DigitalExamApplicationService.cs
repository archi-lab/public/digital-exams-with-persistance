﻿using DigitalExamProject.Config;
using DigitalExamProject.Exams.Domain;
using DigitalExamProject.Students.Application;
using DigitalExamProject.Students.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DigitalExamProject.Exams.Application
{
    public class DigitalExamApplicationService : IDisposable
    {
        private DigitalExamRepositoryService digitalExamRepositoryService;

        public DigitalExamApplicationService()
        {
            DigitalExamsGlobalContext globalContext = new();
            globalContext.Database.EnsureCreated();
            globalContext.SaveChanges();
            digitalExamRepositoryService = new(globalContext);
        }


        public List<DigitalExamDTO> allDigitalExamAsDTOs()
        {
            List<DigitalExam> allExams = digitalExamRepositoryService.GetAll();
            List<DigitalExamDTO> allDTOs = new List<DigitalExamDTO>();
            foreach (DigitalExam digitalExam in allExams)
            {
                allDTOs.Add(DigitalExamDTO.fromEntity(digitalExam));    
            }
            return allDTOs;
        }


        public DigitalExam? FindById(Guid id)
        {
            DigitalExam? exam = digitalExamRepositoryService.FindById(id);
            return exam;
        }



        public DigitalExamDTO? DigitalExamAsDTOs(Guid id)
        {
            DigitalExam? exam = FindById(id);
            if (exam == null) return null;
            return DigitalExamDTO.fromEntity(exam);
        }


        public void SaveChanges()
        {
            digitalExamRepositoryService.SaveChanges();
        }


        public void persist(DigitalExam digitalExam)
        {
            if (digitalExam == null) throw new DigitalExamException("digitalExam == null");
            digitalExamRepositoryService.Add(digitalExam);
            digitalExamRepositoryService.SaveChanges();
        }


        /// <summary>
        /// Bit of a hack, to populate the database for the REST demo
        /// </summary>
        public void InitializeBaseData()
        {
            using StudentApplicationService studentApplicationService = new();
            studentApplicationService.InitializeBaseData();
            string dddIdString = "6ee0626d-0a84-4299-b9d8-d485071100ba";
            DigitalExam ddd = new DigitalExam("DDD", "Einige DDD-Aufgaben", DateTime.Now.AddDays(30), 
                Guid.Parse(dddIdString));
            digitalExamRepositoryService.Add(ddd);
            digitalExamRepositoryService.SaveChanges();
        }


        /// <summary>
        /// Empty the database
        /// </summary>
        public void DeleteAllData()
        {
            digitalExamRepositoryService.DeleteAll();
            using StudentApplicationService studentApplicationService = new();
            studentApplicationService.DeleteAllData();
        }

        public void Dispose()
        {
            digitalExamRepositoryService.Dispose(); 
        }
    }
}
