﻿using DigitalExamProject.Exams.Domain;

namespace DigitalExamProject.Exams.Application
{
    public class SolutionDTO
    {
        public Guid Id { get; set; }
        public int MatrNumber { get; set; }
        public string? Text { get; set; }
        public DateTime? SubmittedAt { get; set; }
        public int Points { get; set; }
        public string? Remark { get; set; }


        public static SolutionDTO? fromEntity(Solution solution)
        {
            if (solution == null) return null;
            SolutionDTO solutionDTO = new();
            solutionDTO.Id = solution.Id;
            solutionDTO.SubmittedAt = solution.SubmittedAt;
            solutionDTO.Text = solution.Text;
            if (solution.Grading != null)
            {
                solutionDTO.Points = solution.Grading.Value.Points;
                solutionDTO.Remark = solution.Grading.Value.Remark;
            }
            if (solution.Student != null)
            {
                solutionDTO.MatrNumber = solution.Student.MatrikelNumber;
            }
            return solutionDTO;
        }
    }



}
