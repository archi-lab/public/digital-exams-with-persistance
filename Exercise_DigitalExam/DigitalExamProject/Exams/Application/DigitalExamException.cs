﻿namespace DigitalExamProject.Exams.Application
{
    public class DigitalExamException : Exception
    {
        public DigitalExamException(string message) : base(message) { }
    }
}
