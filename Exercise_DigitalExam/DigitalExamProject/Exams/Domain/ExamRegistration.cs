﻿using DigitalExamProject.Students.Domain;


namespace DigitalExamProject.Exams.Domain
{
    public class ExamRegistration
    {
        // todo
        public Guid Id { get; private set; }
        public DigitalExam? DigitalExam { get; set; }
        public Student? Student { get; set; }

        public ExamRegistration()
        {
            Id = Guid.NewGuid();
        }
    }
}
