﻿using DigitalExamProject.Students.Domain;

namespace DigitalExamProject.Exams.Domain
{
    public class Solution
    {
        public Guid Id { get; private set; }

        public Grading? Grading { get; set; }

        public string Text { get; private set; }

        public DateTime SubmittedAt { get; private set; }

        public Student? Student { get; private set; }

        protected Solution()
        {
            Id = Guid.NewGuid();
            Text = String.Empty;
            Grading = null;
            SubmittedAt = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        public Solution(Student student, string text) : this()
        {
            Text = text;
            Student = student;
            SubmittedAt = DateTime.Now;
        }


        public Solution(Student student, string text, Guid id) : this(student, text)
        {
            if (id == Guid.Empty) throw new ArgumentNullException(nameof(id));
            this.Id = id;
        }
    }
}
