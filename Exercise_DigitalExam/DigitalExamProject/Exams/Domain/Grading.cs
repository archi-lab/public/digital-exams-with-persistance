﻿namespace DigitalExamProject.Exams.Domain
{
    public struct Grading
    {

        public int Points{ get; private set; }

        public string Remark{ get; private set; }

        public Grading(int points, string remark) 
        {
            Points = points;
            Remark = remark;
        }

        public static Grading FromString(string gradingString)
        {
            if (gradingString == null) throw new ArgumentNullException(nameof(gradingString));
            Grading grading = new Grading();
            string[] subs = gradingString.Split(": ");
            if (subs.Length != 2) throw new InvalidDataException($"Invalid string '{gradingString}'");
            grading.Remark = subs[0];
            grading.Points = Int32.Parse(subs[1]);
            return grading;
        }


        public override string ToString()
        {
            return $"{Remark}: {Points}";
        }



        public override bool Equals(object? obj)
        {
            return obj is Grading address &&
                   Points == address.Points &&
                   Remark == address.Remark;
        }


        public override int GetHashCode()
        {
            return HashCode.Combine(Points, Remark);
        }
    }
}
