﻿using DigitalExamProject.Students.Domain;
using System.Collections.ObjectModel;


namespace DigitalExamProject.Exams.Domain
{
    public class DigitalExam
    {

        public Guid Id { get; private set; }
        
        public string Title { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }

        public ICollection<ExamRegistration> ExamRegistrations { get; set; }

        public ICollection<Solution> SubmittedSolutions { get; private set; }

        public DigitalExam()
        {
            Id = Guid.NewGuid();
            InitializeProperties();
        }


        public DigitalExam(Guid id)
        {
            if (id == Guid.Empty) throw new ArgumentNullException(nameof(id));
            this.Id = id;
            InitializeProperties();
        }


        private void InitializeProperties()
        {
            Title = string.Empty;
            Text = string.Empty;
            Date = DateTime.Now;
            ExamRegistrations = new List<ExamRegistration>();
            SubmittedSolutions = new List<Solution>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="text"></param>
        /// <param name="date"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public DigitalExam(string title, string text, DateTime date) : this()
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Text = text ?? throw new ArgumentNullException(nameof(text));
            Date = date;
        }


        public DigitalExam(string title, string text, DateTime date, Guid id) : this(id)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Text = text ?? throw new ArgumentNullException(nameof(text));
            Date = date;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="student"></param>
        /// <exception cref="InvalidOperationException">Wenn Anmeldung unter einer Woche vor beginn</exception>
        public void Register(Student student)
        {
            var today = DateTime.Now;
            var delta = Date - today;
            if (delta.Days < 7.0) throw new InvalidOperationException("Klausuranmeldung nicht länger möglich");
            ExamRegistration examRegistration = new();
            examRegistration.DigitalExam = this;
            examRegistration.Student = student;
            ExamRegistrations.Add(examRegistration);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="student"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void Unregister(Student student)
        {
            var today = DateTime.Now;
            var delta = Date - today;
            if (delta.Days < 7.0) throw new InvalidOperationException("Klausurabmeldung nicht länger möglich");
            ((List<ExamRegistration>)ExamRegistrations).RemoveAll(er => er.Student.Equals(student));
        }


        public bool IsRegistered(Student student)
        {
            ExamRegistration? examRegistration = ExamRegistrations.FirstOrDefault(er => er.Student.Equals(student));
            return (examRegistration != null);
        }


        public ReadOnlyCollection<Student> GetRegisteredStudents()
        {
            List<Student> students = new();
            foreach (var registration in ExamRegistrations)
            {
                if (registration.Student != null) students.Add(registration.Student);
            }
            return students.AsReadOnly();
        }

        public void SubmitSolution(Student student, string solutionText)
        {
            Solution newSolution = new Solution(student, solutionText);
            SubmittedSolutions.Add(newSolution);
        }

        public ReadOnlyCollection<Solution> GetSolutions()
        {
            return ((List<Solution>)SubmittedSolutions).AsReadOnly();
        }
    }
}
