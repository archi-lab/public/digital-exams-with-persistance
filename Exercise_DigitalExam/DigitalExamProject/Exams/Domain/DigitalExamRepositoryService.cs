﻿using DigitalExamProject.Config;
using Microsoft.EntityFrameworkCore;

namespace DigitalExamProject.Exams.Domain
{
    public class DigitalExamRepositoryService : AbstractRepositoryService
    {
        public DigitalExamRepositoryService(DigitalExamsGlobalContext context)  : base(context) { }

        public DigitalExam? FindById(Guid id)
        {
            return _context.DigitalExams.Find(id);
        }

        public bool ExistsById(Guid id)
        {
            return _context.DigitalExams.Any(digEx => digEx.Id == id);
        }

        public void DeleteById(Guid id)
        {
            DigitalExam? digitalExam = _context.DigitalExams.Find(id);
            if (digitalExam != null)
            {
                _context.DigitalExams.Remove(digitalExam);
                _context.SaveChanges();
            }
        }

        public void DeleteAll() {
            List<DigitalExam> allExams = GetAll();
            allExams.ForEach(digExam => DeleteById(digExam.Id));
        }


        public int CountAll()
        {
            return _context.DigitalExams.Count<DigitalExam>();
        }

        public List<DigitalExam> GetAll()
        {
            return _context.DigitalExams.ToList();
        }

        
    }
}
