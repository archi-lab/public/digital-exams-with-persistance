﻿namespace DigitalExamProject.Students.Domain
{
    public class Address
    {

        public string Street { get; private set; }

        public int Number { get; private set; }

        public int Zip { get; private set; }

        public string City { get; private set; }


        public Address(string street, int number, int zip, string city) 
        {
            Street = street;
            Number = number;
            Zip = zip;
            City = city;
        }


        public override string ToString()
        {
            return $"{Street} {Number}, {Zip} {City}";
        }



        public override bool Equals(object? obj)
        {
            return obj is Address address &&
                   Street == address.Street &&
                   Number == address.Number &&
                   Zip == address.Zip &&
                   City == address.City;
        }


        public override int GetHashCode()
        {
            return HashCode.Combine(Street, Number, Zip, City);
        }
    }
}
