﻿namespace DigitalExamProject.Students.Domain
{
    public class Student
    {

        public Guid Id { get; private set; }
        
        public string Name { set; get; }
        
        public int MatrikelNumber { get; set; }

        public Address? HomeAddress{ get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="matrikelNumber"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public Student(string name, int matrikelNumber)
        {
            Id = Guid.NewGuid();
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.MatrikelNumber = matrikelNumber;
        }

        public Student(string name, int matrikelNumber, Guid id) {
            if (id == Guid.Empty) throw new ArgumentNullException(nameof(id));
            this.Id = id;
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.MatrikelNumber = matrikelNumber;
        }


        public override bool Equals(object? obj)
        {
            return obj is Student student &&
                   Id.Equals(student.Id);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }
    }
}
