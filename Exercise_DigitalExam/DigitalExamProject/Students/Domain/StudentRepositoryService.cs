﻿using DigitalExamProject.Config;
using DigitalExamProject.Exams.Domain;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;

namespace DigitalExamProject.Students.Domain
{
    public class StudentRepositoryService : AbstractRepositoryService
    {
        public StudentRepositoryService(DigitalExamsGlobalContext context) : base(context) { }

        public Student? FindById(Guid id)
        {
            Student? student = _context.Students.Find(id);
            return student;
        }

        public bool ExistsById(Guid id)
        {
            int i = _context.Students.Count();
            return _context.Students.Any(s => s.Id == id);
        }

        public void DeleteById(Guid id)
        {
            Student? student = FindById(id);
            if (student != null) _context.Students.Remove(student);
            _context.SaveChanges();
        }

        public List<Student> FindByName(string name)
        {
            List<Student> students = _context.Students.Where(s => s.Name == name).ToList();
            students.ForEach(s => _context.Attach(s));
            return students;
        }

        public List<Student> FindByMatrNumber(int matrNumber)
        {
            List<Student> students = _context.Students.Where(s => s.MatrikelNumber == matrNumber).ToList();
            students.ForEach(s => _context.Attach(s));
            return students;
        }

        public List<Student> GetAll()
        {
            return _context.Students.ToList();
        }


        public void DeleteAll() {
            List<Student> allStudents = GetAll();
            allStudents.ForEach(student => DeleteById(student.Id));
        }
    }
}
