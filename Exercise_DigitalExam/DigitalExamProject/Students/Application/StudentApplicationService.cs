﻿using DigitalExamProject.Config;
using DigitalExamProject.Exams.Domain;
using DigitalExamProject.Students.Domain;
using Microsoft.EntityFrameworkCore;

namespace DigitalExamProject.Students.Application
{
    public class StudentApplicationService : IDisposable
    {
        private StudentRepositoryService studentRepositoryService;

        public StudentApplicationService()
        {
            DigitalExamsGlobalContext globalContext = new();
            globalContext.Database.EnsureCreated();
            globalContext.SaveChanges();
            studentRepositoryService = new(globalContext);
        }


        public List<Student> FindByName(string name)
        {
            return studentRepositoryService.FindByName(name);
        }


        public List<Student> FindByMatrNumber(int matrNumber)
        {
            return studentRepositoryService.FindByMatrNumber(matrNumber);
        }

        /// <summary>
        /// Bit of a hack, to populate the database for the REST demo
        /// </summary>
        public void InitializeBaseData()
        {
            string amelieIdString = "0e717942-fd8c-49d8-b5f8-84026bc25028";
            string georgIdString = "c964e969-1526-4d4e-9292-5a1dba90dd8b";
            Student amelie = new Student("Amelie", 1234, Guid.Parse(amelieIdString));
            Student georg = new Student("Georg", 5678, Guid.Parse(georgIdString));
            studentRepositoryService.Add(georg);
            studentRepositoryService.Add(amelie);
            studentRepositoryService.SaveChanges();
        }


        /// <summary>
        /// Empty the database
        /// </summary>
        public void DeleteAllData()
        {
            studentRepositoryService.DeleteAll();
        }

        public void Dispose()
        {
            studentRepositoryService.Dispose();
        }

    }
}
