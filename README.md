# Beispiel-Repo "Digitale Klausuren" für C#-Persistenz mit EF Core 6

Dieses Repo zeigt beispielhaft, wie man aus einem einfachen Anforderungstext eine Klassenstruktur 
definieren und dann mittels EF Core 6 in C# als Entities und Value Objects implementieren kann. 
Zur Erstellung des Domain Models gibt auch noch eine 
[längere Beschreibung mit Zwischenschritten](https://www.archi-lab.io/exercises/csharp/digitalExam.html). 

In Kürze ist die Aufgabenstellung die Folgende: 

```
Eine digitale Klausur wird mit einem Titel, einem Aufgabentext und einem Datum erstellt. Die 
Klausur ist dann sofort zur Anmeldung für Studenten offen. Studenten können sich bis eine Woche 
vor der Klausur an- und abmelden, danach nicht mehr. Studenten haben einen Namen und eine 
Matrikel-Nummer. Außerdem kann für sie eine Adresse mit Straße, Hausnummer, Postleitzahl 
und Stadt angegeben sein.

Am Tag der Klausur wird diese manuell gestartet. Die Studierenden können nun eine Lösung 
hochladen, vorausgesetzt, sie sind tatsächlich für die Prüfung angemeldet. Eine Lösung besteht 
aus einem langen Text. Es ist möglich, mehrere Lösungen hochzuladen, alle werden jeweils mit
Datum gespeichert. Nach 2h wird die Klausur manuell beendet, von nun an können die Studenten 
keine Lösungen mehr hochladen. Solange keine Lösung korrigiert ist, kann die Klausur aber
noch einmal geöffnet werden, um mehr Zeit zu gewähren.

Nun beginnt die Korrekturphase. Für jede Lösung wird eine Note gespeichert. Sobald alle 
Lösungen korrigiert sind, gilt die Klausur automatisch als abgeschlossen und die Noten werden 
an das Noten-Management-System übermittelt.
``` 

Analysiert man den Text, kommt man zu folgendem Domain Model mit zwei Aggregates: 
* Aggregate `Exams`
    * `DigitalExam` (Entity, Aggregate Root)
    * `Solution` (Entity)
* Aggregate `Students`
    * `Student` (Entity, Aggregate Root)
    * `Address` (Value Object)

![Domain Model](./digitalExamWithPersistence.png)

Bei Analyse der Abhängigkeiten erkennt man, dass `Exams` eine Referenz auf `Students`
hat, aber nicht umgekehrt. 

![Domain Model](./digitalExamsPackages.png)

Bei den Relationen gibt (fast) alle interessanten Fälle: 
* Kompositionsbeziehung zu einem Value Object (`Student` => `Address`)
* 1:n-Beziehung zwischen zwei Entities (`DigitalExam` => `Solution`)
* n:1-Beziehung (`Solution` => `Student`)
* (ungerichtete) n:m-Beziehung (`DigitalExam` => `Student`)
